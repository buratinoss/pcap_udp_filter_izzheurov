#include <stdio.h>
#include <getopt.h>
#include <vector>
#include "CPcapPacketFilter.h"

static struct option long_options[] =
{
    {"help",  no_argument,       0, 'h'},
    {"address",  required_argument, 0, 'a'},
    {"port",  required_argument, 0, 'p'},
    {0, 0, 0, 0}
};

bool ValidateIpAddress(const std::string &ip_address)
{
    struct sockaddr_in sa;
    int result = inet_pton(AF_INET, ip_address.c_str(), &(sa.sin_addr));
    return result != 0;
}

int main(int argc, char *argv[])
{
    int port = -1;
    int opt;
    int option_index = 0;
    std::string address;
    std::vector<std::string> files;

    while ((opt = getopt_long(argc, argv, "-ha:p:", long_options, &option_index)) != -1)
    {
        switch(opt)
        {
            case 'h': /* --help */
                printf("-a filter by addresses(0.0.0.0)\n -p filter by ports(0-65535)\n");
                break;
            case 'a': /* --address */

                address = optarg;
                if (ValidateIpAddress(address))
                    break;
                else
                {
                    printf("error: address %s validation was failed(0.0.0.0)\n", address.c_str());
                    return -1;
                }
            case 'p': /* --port */
                port = std::stoi(optarg);
                if ((port > 65535) || (port < 0))
                {
                    printf("error: port %d validation was failed(0-65535)\n", port);
                    return -1;
                }
                break;
            case '\1':
            {
                files.push_back(std::string(optarg));
                break;
            }
            case '?':
                break;
            default:
                fprintf(stderr, "invalid option %c\n", opt);
                return 1;
        }
    }

    CPcapPacketFilter filter(address, port);
    for (auto file : files)
    {
        filter.Handler(file);
    }

    return 0;
}