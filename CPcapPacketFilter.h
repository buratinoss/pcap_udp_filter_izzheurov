#ifndef PCAP_TEST_CPCAPPACKETFILTER_H
#define PCAP_TEST_CPCAPPACKETFILTER_H

#include <string>
#include <stdio.h>
#include <stdlib.h>

#include <netinet/in.h>
#include <netinet/ip.h>
#include <net/if.h>
#include <netinet/if_ether.h>
#include <arpa/inet.h>

#include <pcap.h>

struct UDP_hdr
{
    u_short uh_sport;       /* source port */
    u_short uh_dport;       /* destination port */
    u_short uh_ulen;        /* datagram length */
    u_short uh_sum;         /* datagram checksum */
};

class CPcapPacketFilter
{
    public:
        CPcapPacketFilter(std::string address, int port);
        CPcapPacketFilter();

        /*
         * @brief handler of file
         */
        bool Handler(std::string _file_name);

        void SetAddressForFilter(const std::string &m_address_for_filter);
        void SetPortForFilter(int port);
    private:
        std::string m_address_for_filter;
        int m_port_for_filter;

    private:
        /*
         * @brief Returns a string representation of a timestamp.
         */
        const char *TimestampString(struct timeval ts);

        /*
         * @brief parses a packet
         * @param ts argument is the timestamp associated with the packet
         * @param capture_len is the length of the packet *as captured by the tracing program*
         */
        void DumpUDPPacket(const unsigned char *packet, struct timeval ts, unsigned int capture_len);

        /*
         * @breif filter by address & port
         */
        bool SuitableValues(std::string address, int port);
};


#endif //PCAP_TEST__CPCAPPACKETFILTER_H
