#include "CPcapPacketFilter.h"

CPcapPacketFilter::CPcapPacketFilter()
{
}

CPcapPacketFilter::CPcapPacketFilter(std::string address, int port) : m_address_for_filter(address), m_port_for_filter(port)
{
}

bool CPcapPacketFilter::Handler(std::string _file_name)
{
    pcap_t *pcap;
    const unsigned char *packet;
    char errbuf[PCAP_ERRBUF_SIZE];
    struct pcap_pkthdr header;

    pcap = pcap_open_offline(_file_name.c_str(), errbuf);
    if (pcap == NULL)
    {
        fprintf(stderr, "error reading pcap file: %s\n", errbuf);
        return false;
    }

    while ((packet = pcap_next(pcap, &header)) != NULL)
        DumpUDPPacket(packet, header.ts, header.caplen);

    return true;

}

const char* CPcapPacketFilter::TimestampString(struct timeval ts)
{
    static char timestamp_string_buf[256];
    sprintf(timestamp_string_buf, "%d.%06d", (int) ts.tv_sec, (int) ts.tv_usec);
    return timestamp_string_buf;
}

void CPcapPacketFilter::DumpUDPPacket(const unsigned char *packet, struct timeval ts, unsigned int capture_len)
{
    struct ip *ip;
    struct UDP_hdr *udp;
    unsigned int IP_header_length;

    /* For simplicity, we assume Ethernet encapsulation.
     * We didn't even capture a full Ethernet header, so we
     * can't analyze this any further.
     */
    if (capture_len < sizeof(struct ether_header))
        return;

    /* Skip over the Ethernet header. */
    packet += sizeof(struct ether_header);
    capture_len -= sizeof(struct ether_header);

    /* Didn't capture a full IP header */
    if (capture_len < sizeof(struct ip))
        return;

    ip = (struct ip *) packet;

    IP_header_length = ip->ip_hl * 4;    /* ip_hl is in 4-byte words */

    /* didn't capture the full IP header including options */
    if (capture_len < IP_header_length)
        return;

    if (ip->ip_p != IPPROTO_UDP)
        return;

    /* Skip over the IP header to get to the UDP header. */
    packet += IP_header_length;
    capture_len -= IP_header_length;

    if (capture_len < sizeof(struct UDP_hdr))
        return;

    udp = (struct UDP_hdr *) packet;

    if (SuitableValues(inet_ntoa(ip->ip_dst), ntohs(udp->uh_dport)))
        printf("%s %s %d %d\n", TimestampString(ts), inet_ntoa(ip->ip_dst), ntohs(udp->uh_dport), ntohs(udp->uh_ulen));

    return;
}

bool CPcapPacketFilter::SuitableValues(std::string address, int port)
{
    bool result = true;

    if (!m_address_for_filter.empty() && (address != m_address_for_filter))
        result = false;

    if ((m_port_for_filter != -1) && ((port != m_port_for_filter)) && result)
        result = false;

    return result;
}

void CPcapPacketFilter::SetAddressForFilter(const std::string &address_for_filter)
{
    m_address_for_filter = address_for_filter;
}

void CPcapPacketFilter::SetPortForFilter(int port)
{
    m_port_for_filter = port;
}

