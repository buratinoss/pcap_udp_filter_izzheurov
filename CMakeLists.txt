cmake_minimum_required(VERSION 3.6)
project(pcap_test)

set(CMAKE_CXX_STANDARD 11)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -lpcap")

set(SOURCE_FILES main.cpp CPcapPacketFilter.cpp CPcapPacketFilter.h)

add_executable(pcap_test ${SOURCE_FILES})